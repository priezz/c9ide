# Install
```
brew tap priezz/tap
brew install c9ide
```

# Usage
```
c9ide <path-to-your-project>
c9ide .
```

# Update
```
brew update
brew upgrade c9ide
```

